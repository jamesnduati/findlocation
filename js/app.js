// Relevant API docs to get you started:
// http://parseplatform.org/Parse-SDK-JS/api/classes/Parse.Cloud.html
// http://parseplatform.org/Parse-SDK-JS/api/classes/Parse.Object.html
// http://parseplatform.org/Parse-SDK-JS/api/classes/Parse.File.html
// http://parseplatform.org/Parse-SDK-JS/api/classes/Parse.Query.html
// http://parseplatform.org/Parse-SDK-JS/api/classes/Parse.Promise.html
// http://parseplatform.org/Parse-SDK-JS/api/classes/Parse.User.html
// http://parseplatform.org/Parse-SDK-JS/api/classes/Parse.Error.html

var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// express middleware
// define endpoints here

app.post('/echo', function(req,res) {
	res.send('echo echo echo');
});


// This endpoint takes in a username (phone), and a password and authenticates the user.
//
// Returns userId, sessionToken if able to login, an error otherwise.
// The userId, sessionToken pair retrieved from this call are to be used for every single interaction with
// this server. 
app.post('/auth', function(req,res) {
	var username = req.body.username;
  	var password = req.body.password;
  	// checking for empty fields
    if (!username.trim() && !password.trim()) {
    	 var errors = "Please set the required values"
    	res.send(errors);
    }
  	// Call Parse Login function with those variables
    Parse.User.logIn(username, password, {
        // If the username and password matches
        success: function(user) {
        	var data = {
        			"staus":"success",
        			"user": user
        		}
            res.send(data);	
        },
        // If there is an error
        error: function(user, error) {
            res.send(error);	
        }
    });
});

// input params: userId, sessionToken, phone
// validate the sessionToken against the userId provided, if they don't match return an error
//
// Fetch the top 4 addresses of the user matching User.username==phone
// Search for addresses within UserAddressLayer table (i.e UserAddressLayer.owner == User matching the provided phone number)
// return the addresses in order of most recently updated (use UserAddressLayer.updatedAt timestamp for sorting)
// 
app.post('/getAddresses', function(req,res) {
	var userId = req.body.userId;
	var sessionToken = req.body.sessionToken
  	var phone = req.body.mobile;
  	// checking for empty fields
    if (!userId.trim() && !sessionToken.trim() && !phone.trim()) {
    	var errors = "Please set the required values"
    	res.send(errors);
    }
    // query the user model 
	var query = new Parse.Query(Parse.User);
	query.get(userId, {
		useMasterKey: true,
		  success: function(user) {
		    // object is an instance of Parse.Object.
		    // checkig the session token
		    if (user.getSessionToken) {}
		    res.send(user);
		  },

		  error: function(user, err) {
		    // error is an instance of Parse.Error.
		    res.send(500, err);
		  }
	});

});

// input params: userId, sessionToken, 
// ualId - id / ObjectId of the Address card selected (i.e UserAddressLayer.id/objectId)
// returns checkoutId, deliveryId within a json object i.e {"checkoutId": "xyz", "deliveryId": "rty" }
// create a checkout as per data dashboard Checkouts table
// create a delviery as per data dashboard Delivery table
// delivery table should have a pointer to checkouts and vise versa
// both delivery and checkouts table should have pointers to UAL (UserAddressLayer)
// please reffer to the google doc as well for more information.
app.post('/checkout', function(req,res) {

});